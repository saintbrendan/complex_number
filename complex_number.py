class ComplexNumber:
    def __init__(self, real, imaginary):
        self.real, self.imaginary = real, imaginary

    def add(self, cx1):
        return ComplexNumber(self.real + cx1.real, self.imaginary + cx1.imaginary)

    def multiply(self, cx1):
        real = self.real * cx1.real - self.imaginary * cx1.imaginary
        i = self.real * cx1.imaginary + self.imaginary * cx1.real
        return ComplexNumber(real, i)

    def __str__(self):
        return "r:{} i:{}".format(self.real, self.imaginary)


def main():
    cx1 = ComplexNumber(1, 70)
    doubled = cx1.add(cx1)
    print(doubled)


if __name__ == '__main__':
    main()
