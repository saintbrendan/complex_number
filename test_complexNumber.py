from unittest import TestCase

from complex_number import ComplexNumber


class TestComplexNumber(TestCase):
    def test_add(self):
        a = ComplexNumber(1, 2)
        b = ComplexNumber(70, 90)
        sum = a.add(b)
        self.assertAlmostEqual(71, sum.real)
        self.assertAlmostEqual(92, sum.imaginary, )

    def test_multiply(self):
        a = ComplexNumber(12, 5)
        b = ComplexNumber(11, 7)
        product = a.multiply(b)
        self.assertAlmostEqual(97, product.real)
        self.assertAlmostEqual(139, product.imaginary, )
